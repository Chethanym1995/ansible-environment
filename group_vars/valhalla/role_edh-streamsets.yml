# ===========================================================================
# START EDH-STREAMSETS ROLE VARIABLES
# ===========================================================================
#
#=========REQUIRED VARIABLES
#---groupvars__edh_api_hostname                       <--- first host listed under 'manager' group in inventory file
#---groupvars__edh_api_username                       <--- the cloudera manager username
#---groupvars__edh_api_password                       <--- the cloudera manager password (placed in ansible-vault)
#---groupvars__edh_api_protocol                       <--- http/https
#---groupvars__edh_api_port                           <--- 7183
#---groupvars__edh_api_version                        <--- the cloudera manager api version, example: v19
#---groupvars__edh_api_cluster                        <--- the cluster we are managing within cloudera manager
#---groupvars__edh_generategroupvars_location:        <--- where you want groupvars files to be generated; "/tmp/"
#---groupvars__edh_generategroupvars_user:            <--- user who owns generated groupvars files; "root"
#---groupvars__edh_generategroupvars_group:           <--- group who owns generated groupvars files; "root"

#---short url for re-use in tasks
edh_streamsets__url: "{{ groupvars__edh_api_protocol }}://{{ groupvars__edh_api_hostname }}:{{ groupvars__edh_api_port }}/api/{{ groupvars__edh_api_version }}/clusters/{{ groupvars__edh_api_cluster }}"

#---the service name, in some environments deployed by director, or cloudera manager instances managing more than 1 cluster we need to change this
edh_streamsets__service_name: "streamsets"

# ===========================================================================
#=========================================== SERVICE


#---Enable Kerberos Client
#-----Runs the Data Collector within a Kerberos session which is propagated to all stagelibs
edh_streamsets__service__kerberos_client_enabled: "true"
# default: "false"
# phdata: "true"


# ===========================================================================
#=========================================== DATACOLLECTOR

#========START GROUP: ( https* ) HTTPS Configuration

#---Java KeyStore Password on Worker node
#-----Password for keystore file on worker.
edh_streamsets__role_datacollector__https_cluster_keystore_password: "{{ vaultvars__keystore_password }}"
# default: "${file("/opt/security/jks/keystore-password.txt")}"

#---Java KeyStore Path on Worker node
#-----Path to keystore file on worker node. This should always be an absolute location.
edh_streamsets__role_datacollector__https_cluster_keystore_path: "/opt/cloudera/security/pki/jks/local.jks"
# default: "/opt/security/jks/sdc-keystore.jks"

#---Java KeyStore Password
#-----Password for Java Keystore file
edh_streamsets__role_datacollector__https_keystore_password: "{{ vaultvars__keystore_password }}"
# default: ""

#---Java KeyStore Path
#-----Path to Java keystore file
edh_streamsets__role_datacollector__https_keystore_path: "/opt/cloudera/security/pki/jks/local.jks"
# default: "keystore.jks"

#---Secure Web UI Port
#-----The port that will be used for the Data Collector Web UI over HTTPS. If set to a value other than -1, the SDC will run SSL on this port
edh_streamsets__role_datacollector__https_port: "18631"
# default: "-1"

#========STOP GROUP: ( https* ) HTTPS Configuration

#========START GROUP: ( ldap* ) LDAP Configuration

#---LDAP Bind DN
#-----The distinguished name to use for binding to LDAP for authentiction.
edh_streamsets__role_datacollector__ldap_bindDn: "clouderagroupsync@phdata.io"
# default: ""

#---LDAP Bind Password
#-----The password to use for binding to LDAP for authentiction.
edh_streamsets__role_datacollector__ldap_bindPassword: "{{ vaultvars__ldap_bind_password }}"
# default: ""

#---Enable LDAP Debug Logging
#-----Enable debug logging for LDAP connections.
edh_streamsets__role_datacollector__ldap_debug: "true"
# default: "false"
# phdata: "true"

#---Force Binding Login for LDAP
#-----Determines if binding login checks are performed.
edh_streamsets__role_datacollector__ldap_forceBindingLogin: "true"
# default: "false"

#---LDAP Host
#-----LDAP server host name.
edh_streamsets__role_datacollector__ldap_hostname: "ad1.valhalla.phdata.io"
# default: ""

#---LDAP Port
#-----LDAP server port. Typically 389 for LDAP and 636 for LDAPS.
edh_streamsets__role_datacollector__ldap_port: "389"
# default: "389"

#---LDAP Role Base DN
#-----Base DN to search for role membership.
edh_streamsets__role_datacollector__ldap_roleBaseDn: "OU=groups,OU=Hadoop,DC=phdata,DC=io"
# default: "ou=groups,dc=example,dc=com"

#---LDAP Role Member Attribute
#-----Name of the role attribute for user names.
edh_streamsets__role_datacollector__ldap_roleMemberAttribute: "member"
# default: "uniqueMember"
# phdata: "member"

#---LDAP Role Object Class
#-----Name of the Role object class.
edh_streamsets__role_datacollector__ldap_roleObjectClass: "group"
# default: "groupOfUniqueNames"
# phdata: "group"

#---Use LDAPS Protocol
#-----Enables using LDAP over SSL.
edh_streamsets__role_datacollector__ldap_useLdaps: "false"
# default: "false"
# phdata: "true"

#---LDAP User Base DN
#-----Base DN under which user accounts are located.
edh_streamsets__role_datacollector__ldap_userBaseDn: "OU=users,OU=Hadoop,DC=phdata,DC=io"
# default: "ou=people,dc=company"

#---Filter to search a user
#-----Filter to be used in ldapsearch query. Default is uid={user}, where {user} will be replaced with the username user entered to login to SDC.
edh_streamsets__role_datacollector__ldap_userFilter: "sAMAccountName={user}"
# default: "uid={user}"
# phdata: "sAMAccountName={user}"

#---LDAP User ID Attribute
#-----Name of the user ID attribute.
edh_streamsets__role_datacollector__ldap_userIdAttribute: "uid"
# default: "uid"
# phdata: "sAMAccountName"

#---LDAP User Object Class
#-----Name of the user object class.
edh_streamsets__role_datacollector__ldap_userObjectClass: "user"
# default: "inetOrgPerson"
# phdata: "person"

#---LDAP Username Attribute
#-----Name of the username attribute.
edh_streamsets__role_datacollector__ldap_userRdnAttribute: "uid"
# default: "uid"
# phdata: "sAMAccountName"

#========STOP GROUP: ( ldap* ) LDAP Configuration

#========START GROUP: ( http* ) LDAP Auth Role Mapping

#---HTTP Authentication LDAP Role Mapping
#-----Mapping between LDAP groups and StreamSets Roles. The mapping is specified as the following pattern: &ltldap-group&gt:&ltsdc-role&gt(,&ltsdc-role&gt)*(;&ltldap-group&gt:&ltsdc-role&gt(,&ltsdc-role&gt)*)*. e.g. Administrator:admin;Manager:manager;DevOP:creator;Tester:guest;
edh_streamsets__role_datacollector__http_authentication_ldap_role_mapping: "edh_user:admin"
# default: ""

#---HTTP Authentication Login Module
#-----For 'file', the authentication and role information is read from a property file (etc/basic-realm.properties, etc/digest-realm.properties or etc/form-realm.properties based on the 'http.authentication' value). For 'ldap', the authentication and role information is read from a LDAP or Active Directory server.
edh_streamsets__role_datacollector__http_authentication_login_module: "file"
# default: "file"
# phdata: "ldap"

#========STOP GROUP: ( http* ) LDAP Auth Role Mapping


#---Data Collector Logging Threshold
#-----The minimum log level for Data Collector logs
edh_streamsets__role_datacollector__log_threshold: "INFO"
# default: "INFO"
# phdata: "WARN"

#---SMTP Host name
#-----Hostname of the SMTP server.
edh_streamsets__role_datacollector__mail_smtp_host: "localhost"
# default: "localhost"

#---Email protocol
#-----Use smtp or smtps.
edh_streamsets__role_datacollector__mail_transport_protocol: "smtp"
# default: "smtp"

#---Automatically Restart Process
#-----When set, this role's process is automatically (and transparently) restarted in the event of an unexpected failure.
edh_streamsets__role_datacollector__process_auto_restart: "false"
# default: "false"
# phdata: "true"

#---Header title
#-----Optional text to display in the Data Collector console next to the StreamSets logo.
edh_streamsets__role_datacollector__ui_header_title: "phData"
# default: ""

#---Verify Vault TLS Connections
#-----Disabling will not verify certificate validity when connecting to Vault over TLS
edh_streamsets__role_datacollector__vault_ssl_verify: "false"
# default: "false"

#---Mail from address
#-----Email address to send alert email.
edh_streamsets__role_datacollector__xmail_from_address: "sdc@localhost"
# default: "sdc@localhost"

# ===========================================================================
# END EDH-STREAMSETS ROLE VARIABLES
# ===========================================================================
