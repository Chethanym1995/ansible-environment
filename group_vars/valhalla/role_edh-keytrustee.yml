# ===========================================================================
# START EDH-KEYTRUSTEE ROLE VARIABLES
# ===========================================================================
#
#=========REQUIRED VARIABLES
#---groupvars__edh_api_hostname                       <--- first host listed under 'manager' group in inventory file
#---groupvars__edh_api_username                       <--- the cloudera manager username
#---groupvars__edh_api_password                       <--- the cloudera manager password (placed in ansible-vault)
#---groupvars__edh_api_protocol                       <--- http/https
#---groupvars__edh_api_port                           <--- 7183
#---groupvars__edh_api_version                        <--- the cloudera manager api version, example: v19
#---groupvars__edh_api_cluster                        <--- the cluster we are managing within cloudera manager
#---groupvars__edh_generategroupvars_location:        <--- where you want groupvars files to be generated; "/tmp/"
#---groupvars__edh_generategroupvars_user:            <--- user who owns generated groupvars files; "root"
#---groupvars__edh_generategroupvars_group:           <--- group who owns generated groupvars files; "root"

#---short url for re-use in tasks
edh_keytrustee__url: "{{ groupvars__edh_api_protocol }}://{{ groupvars__edh_api_hostname }}:{{ groupvars__edh_api_port }}/api/{{ groupvars__edh_api_version }}/clusters/{{ groupvars__edh_api_cluster }}"

#---the service name, in some environments deployed by director, or cloudera manager instances managing more than 1 cluster we need to change this
edh_keytrustee__service_name: "keytrustee"

# ===========================================================================
#=========================================== SERVICE


#---ZooKeeper Authentication Type for Secret Signer
#-----ZooKeeper Authentication for Secret Signer. Can be either "none" or "sasl"
edh_keytrustee__service__hadoop_kms_authentication_signer_secret_provider_zookeeper_auth_type: "sasl"
# default: "none"
# phdata: "sasl"

#---Authentication Type
#-----Authentication type for the KMS. Can either be "simple" or "kerberos".
edh_keytrustee__service__hadoop_kms_authentication_type: "kerberos"
# default: "simple"
# phdata: "kerberos"

#---ZooKeeper Service
#-----Name of the ZooKeeper service that this Key Trustee KMS service instance depends on
edh_keytrustee__service__zookeeper_service: "zookeeper"
# default: ""


# ===========================================================================
#=========================================== KMS_KEYTRUSTEE

#========START GROUP: ( ssl_* ) KMS Proxy TLS/SSL

#---Key Management Server Proxy TLS/SSL Certificate Trust Store File
#-----The location on disk of the trust store, in .jks format, used to confirm the authenticity of TLS/SSL servers that Key Management Server Proxy might connect to. This is used when Key Management Server Proxy is the client in a TLS/SSL connection. This trust store must contain the certificate(s) used to sign the service(s) connected to. If this parameter is not provided, the default list of well-known certificate authorities is used instead.
edh_keytrustee__role_kms_keytrustee__ssl_client_truststore_location: "/opt/cloudera/security/pki/jks/jssecacerts-java-1.8.jks"
# default: ""

#---Key Management Server Proxy TLS/SSL Certificate Trust Store Password
#-----The password for the Key Management Server Proxy TLS/SSL Certificate Trust Store File. This password is not required to access the trust store; this field can be left blank. This password provides optional integrity checking of the file. The contents of trust stores are certificates, and certificates are public information.
edh_keytrustee__role_kms_keytrustee__ssl_client_truststore_password: "{{ vaultvars__truststore_password }}"
# default: ""

#---Enable TLS/SSL for Key Management Server Proxy
#-----Encrypt communication between clients and Key Management Server Proxy using Transport Layer Security (TLS) (formerly known as Secure Socket Layer (SSL)).
edh_keytrustee__role_kms_keytrustee__ssl_enabled: "true"
# default: "false"

#---Key Management Server Proxy TLS/SSL Server JKS Keystore File Location
#-----The path to the TLS/SSL keystore file containing the server certificate and private key used for TLS/SSL. Used when Key Management Server Proxy is acting as a TLS/SSL server. The keystore must be in JKS format.
edh_keytrustee__role_kms_keytrustee__ssl_server_keystore_location: "/opt/cloudera/security/pki/jks/local.jks"
# default: ""

#---Key Management Server Proxy TLS/SSL Server JKS Keystore File Password
#-----The password for the Key Management Server Proxy JKS keystore file.
edh_keytrustee__role_kms_keytrustee__ssl_server_keystore_password: "{{ vaultvars__keystore_password }}"
# default: ""

#========STOP GROUP: ( ssl_* ) KMS Proxy TLS/SSL


#---Key Management Server Proxy Advanced Configuration Snippet (Safety Valve) for kms-acls.xml
#-----For advanced use only. A string to be inserted into <strong>kms-acls.xml</strong> for this role only.
edh_keytrustee__role_kms_keytrustee__kms_acls_xml_role_safety_valve: |
  <property><name>hadoop.kms.acl.CREATE</name><value>brock,adam,andy,dsbluml edh_admin_full</value><description>
    ACL for create-key operations.
    If the user is not in the GET ACL, the key material is not returned
    as part of the response.
  </description></property><property><name>hadoop.kms.acl.DELETE</name><value>brock,adam,andy,dsbluml edh_admin_full</value><description>
    ACL for delete-key operations.
  </description></property><property><name>hadoop.kms.acl.ROLLOVER</name><value>brock,adam,andy,dsbluml edh_admin_full</value><description>
    ACL for rollover-key operations.
    If the user does is not in the GET ACL, the key material is not returned
    as part of the response.
  </description></property><property><name>hadoop.kms.acl.GET</name><value></value><description>
    ACL for get-key-version and get-current-key operations.
  </description></property><property><name>hadoop.kms.acl.GET_KEYS</name><value>brock,adam,andy,dsbluml edh_admin_full</value><description>
    ACL for get-keys operations.
  </description></property><property><name>hadoop.kms.acl.SET_KEY_MATERIAL</name><value></value><description>
    Complementary ACL for CREATE and ROLLOVER operations to allow the client
    to provide the key material when creating or rolling a key.
  </description></property><property><name>hadoop.kms.acl.GENERATE_EEK</name><value>hdfs supergroup</value><description>
    ACL for generateEncryptedKey CryptoExtension operations.
  </description></property><property><name>hadoop.kms.blacklist.CREATE</name><value>hdfs supergroup</value></property><property><name>hadoop.kms.blacklist.DELETE</name><value>hdfs supergroup</value></property><property><name>hadoop.kms.blacklist.ROLLOVER</name><value>hdfs supergroup</value></property><property><name>hadoop.kms.blacklist.GET</name><value>*</value></property><property><name>hadoop.kms.blacklist.GET_KEYS</name><value></value></property><property><name>hadoop.kms.blacklist.SET_KEY_MATERIAL</name><value>*</value></property><property><name>hadoop.kms.blacklist.DECRYPT_EEK</name><value>hdfs supergroup</value></property><property><name>keytrustee.kms.acl.UNDELETE</name><value>dsbluml </value><description>
    ACL that grants access to the UNDELETE operation on all keys.
    Only used by Key Trustee KMS.
  </description></property><property><name>keytrustee.kms.acl.PURGE</name><value>dsbluml </value><description>
    ACL that grants access to the PURGE operation on all keys.
    Only used by Key Trustee KMS.
  </description></property><property><name>default.key.acl.MANAGEMENT</name><value></value><description>
    Default ACL that grants access to the MANAGEMENT operation on all keys.
  </description></property><property><name>default.key.acl.GENERATE_EEK</name><value></value><description>
    Default ACL that grants access to the GENERATE_EEK operation on all keys.
  </description></property><property><name>default.key.acl.DECRYPT_EEK</name><value></value><description>
    Default ACL that grants access to the DECRYPT_EEK operation on all keys.
  </description></property><property><name>default.key.acl.READ</name><value></value><description>
    Default ACL that grants access to the READ operation on all keys.
  </description></property><property><name>whitelist.key.acl.MANAGEMENT</name><value>brock,adam,andy,dsbluml edh_admin_full</value><description>
    Whitelist ACL for MANAGEMENT operations for all keys.
  </description></property><property><name>whitelist.key.acl.READ</name><value>hdfs,brock,adam,andy,dsbluml edh_admin_full,supergroup</value><description>
    Whitelist ACL for READ operations for all keys.
  </description></property><property><name>whitelist.key.acl.GENERATE_EEK</name><value>hdfs supergroup</value><description>
    Whitelist ACL for GENERATE_EEK operations for all keys.
  </description></property><property><name>whitelist.key.acl.DECRYPT_EEK</name><value>brock,adam,andy,dsbluml edh_admin_full,edh_user</value><description>
    Whitelist ACL for DECRYPT_EEK operations for all keys.
  </description></property><property><name>key.acl.hive-key.DECRYPT_EEK</name><value>hive hive</value><description>
    Gives the hive user and the hive group access to the key named "hive-key".
    This allows the hive service to read and write files in /user/hive/.
    Also note that the impala user ought to be a member of the hive group
    in order to enjoy this same access.
  </description></property><property><name>key.acl.hive-key.READ</name><value>hive hive</value><description>
    Required because hive compares key strengths when joining tables.
  </description></property><property><name>key.acl.hbase-key.DECRYPT_EEK</name><value>hbase hbase</value><description>
    Gives the hbase user and hbase group access to the key named "hbase-key".
    This allows the hbase service to read and write files in /hbase.
  </description></property><property><name>key.acl.solr-key.DECRYPT_EEK</name><value>solr solr</value><description>
    Gives the solr user and solr group access to the key named "solr-key".
    This allows the solr service to read and write files in /solr.
  </description></property><property><name>key.acl.mapred-key.DECRYPT_EEK</name><value>yarn,mapred hadoop</value><description>
    Gives the mapred user and mapred group access to the key named "mapred-key".
    This allows mapreduce to read and write files in /user/history.
    This is required by YARN.
  </description></property><property><name>key.acl.hue-key.DECRYPT_EEK</name><value>oozie,hue oozie,hue</value><description>
    Gives the appropriate users and groups access to the key named "hue-key".
    This allows hue and oozie to read and write files in /user/hue.
    Oozie is required here because it will attempt to access workflows in
    /user/hue/oozie/workspaces.
  </description></property>
# default: ""
# phdata: |
# <property><name>hadoop.kms.acl.CREATE</name><value> {{ keytrustee_admin_acl_group }}</value><description>
#   ACL for create-key operations.
#   If the user is not in the GET ACL, the key material is not returned
#   as part of the response.
# </description></property>
# <property><name>hadoop.kms.acl.DELETE</name><value> {{ keytrustee_admin_acl_group }}</value><description>
#   ACL for delete-key operations.
# </description></property>
# <property><name>hadoop.kms.acl.ROLLOVER</name><value> {{ keytrustee_admin_acl_group }}</value><description>
#   ACL for rollover-key operations.
#   If the user does is not in the GET ACL, the key material is not returned
#   as part of the response.
# </description></property>
# <property><name>hadoop.kms.acl.GET</name><value>hive hive</value><description>
#   ACL for get-key-version and get-current-key operations.
# </description></property>
# <property><name>hadoop.kms.acl.GET_KEYS</name><value> {{ keytrustee_admin_acl_group }}</value><description>
#   ACL for get-keys operations.
# </description></property>
# <property><name>hadoop.kms.acl.SET_KEY_MATERIAL</name><value></value><description>
#   Complementary ACL for CREATE and ROLLOVER operations to allow the client
#   to provide the key material when creating or rolling a key.
# </description></property>
# <property><name>hadoop.kms.acl.GENERATE_EEK</name><value>hdfs supergroup</value><description>
#   ACL for generateEncryptedKey CryptoExtension operations.
# </description></property>
# <property><name>hadoop.kms.blacklist.CREATE</name><value>hdfs supergroup</value></property><property><name>hadoop.kms.blacklist.DELETE</name><value>hdfs supergroup</value></property><property><name>hadoop.kms.blacklist.ROLLOVER</name><value>hdfs supergroup</value></property><property><name>hadoop.kms.blacklist.GET</name><value>*</value></property><property><name>hadoop.kms.blacklist.GET_KEYS</name><value></value></property><property><name>hadoop.kms.blacklist.SET_KEY_MATERIAL</name><value>*</value></property><property><name>keytrustee.kms.acl.UNDELETE</name><value></value><description>
#   ACL that grants access to the UNDELETE operation on all keys.
#   Only used by Key Trustee KMS.
# </description></property>
# <property><name>keytrustee.kms.acl.PURGE</name><value></value><description>
#   ACL that grants access to the PURGE operation on all keys.
#   Only used by Key Trustee KMS.
# </description></property>
# <property><name>default.key.acl.MANAGEMENT</name><value></value><description>
#   Default ACL that grants access to the MANAGEMENT operation on all keys.
# </description></property>
# <property><name>default.key.acl.GENERATE_EEK</name><value></value><description>
#   Default ACL that grants access to the GENERATE_EEK operation on all keys.
# </description></property>
# <property><name>default.key.acl.DECRYPT_EEK</name><value></value><description>
#   Default ACL that grants access to the DECRYPT_EEK operation on all keys.
# </description></property>
# <property><name>default.key.acl.READ</name><value></value><description>
#   Default ACL that grants access to the READ operation on all keys.
# </description></property>
# <property><name>whitelist.key.acl.MANAGEMENT</name><value> {{ keytrustee_admin_acl_group }}</value><description>
#   Whitelist ACL for MANAGEMENT operations for all keys.
# </description></property>
# <property><name>whitelist.key.acl.READ</name><value>hdfs,hive supergroup,hive,{{ keytrustee_admin_acl_group }}</value><description>
#   Whitelist ACL for READ operations for all keys.
# </description></property>
# <property><name>whitelist.key.acl.GENERATE_EEK</name><value>hdfs supergroup</value><description>
#   Whitelist ACL for GENERATE_EEK operations for all keys.
# </description></property>
# <property><name>whitelist.key.acl.DECRYPT_EEK</name><value>hdfs,hive,solr,yarn,mapred,oozie,hue,spark hive,solr,hadoop,oozie,hue,{{ keytrustee_admin_acl_group }},{{ keytrustee_user_acl_group }}</value><description>
#   Whitelist ACL for DECRYPT_EEK operations for all keys.
# </description></property>
# <property><name>hadoop.kms.blacklist.DECRYPT_EEK</name><value>hdfs supergroup</value></property>


#---Key Management Server Proxy Advanced Configuration Snippet (Safety Valve) for kms-site.xml
#-----For advanced use only. A string to be inserted into <strong>kms-site.xml</strong> for this role only.
edh_keytrustee__role_kms_keytrustee__kms_site_xml_role_safety_valve: ""
# default: ""
# phdata: "<property><name>hadoop.kms.proxyuser.sdc.groups</name><value>*</value><description>allow sdc user to proxy to other users</description></property><property><name>hadoop.kms.proxyuser.sdc.hosts</name><value>*</value><description>allow sdc user to proxy from these hosts</description></property>"

#---KMS Heap Size
#-----Maximum heap size of the KMS.
edh_keytrustee__role_kms_keytrustee__kms_heap_size: "67108864"
# default: "1073741824"

#---Automatically Restart Process
#-----When set, this role's process is automatically (and transparently) restarted in the event of an unexpected failure.
edh_keytrustee__role_kms_keytrustee__process_auto_restart: "false"
# default: "false"
# phdata: "true"

# ===========================================================================
# END EDH-KEYTRUSTEE ROLE VARIABLES
# ===========================================================================
