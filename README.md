# Ansible for Valhalla

## How to contribute
* Create a branch associated with your change
* Ensure `--check` mode bases via the [Precommit](https://edge1.valhalla.phdata.io:8000/project/Ansible_valhalla/job/show/cf078ad3-8442-430e-95a5-e20cb2e84d18) job
* Perform a pull request to master
* Gain approval by one member of the ansible team
* Merge using fast-forward or squash
* Ensure the [Postcommit](https://edge1.valhalla.phdata.io:8000/project/Ansible_valhalla/job/show/e27da117-afb0-460b-9623-bcb7623ed956) job passes

## Useful Reading
* [Rundeck Ansible-valhalla Design Document](https://docs.google.com/document/d/1RnS7Lw0NWiVq2CZHUfzLIqSSYbXpNk_PZ4UTerJThkY/edit)
* [Ansible - Scalable Deployment Implementation](https://docs.google.com/document/d/1eVxyWas6DsJ2jL4RnIK3dNc-9DXSFTy-lNkFak3MU8I/edit#)

## Directory Layout

```
http://docs.ansible.com/ansible/playbooks_best_practices.html
```

When using anaconda.yml you need to run ansible-galaxy install andrewrothstein.anaconda on your ansible node first.

\*.inventory              <-- cluster specific inventories

group_vars/
   group1                 <-- here we assign variables to particular groups
   group2                 <-- ""
host_vars/
   hostname1              <-- if systems need specific variables, put them here
   hostname2              <-- ""

library/                  <-- if any custom modules, put them here (optional)

site.yml                  <-- master playbook

playbooks/                <-- playbooks for adhoc automation

roles/
    common/               <-- this hierarchy represents a "role"
        tasks/            <--
            main.yml      <-- tasks file can include smaller files if warranted
        handlers/         
            main.yml      <-- handlers file
        templates/        <-- files for use with the template resource
            ntp.conf.j2   <------- templates end in .j2
        files/            
            bar.txt       <-- files for use with the copy resource
            foo.sh        <-- script files for use with the script resource
        vars/             
            main.yml      <-- variables associated with this role
        defaults/         
            main.yml      <-- default lower priority variables for this role
        meta/             
            main.yml      <-- role dependencies
