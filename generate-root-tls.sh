#!/bin/bash -e
set -x
USAGE="Usage: generate-root-tls <inventory-file> <password> <dns suffix>"
# Generates Cluster CAA signed keys for all files in the inventory file
# Generates root Cluster CA if needed

# find where I live
# Reference: http://stackoverflow.com/questions/59895/can-a-bash-script-tell-what-directory-its-stored-in

SOURCE="${BASH_SOURCE[0]}"
BIN_DIR="$( dirname "$SOURCE" )"
while [ -h "$SOURCE" ]
do
  SOURCE="$(readlink "$SOURCE")"
  [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
  BIN_DIR="$( cd -P "$( dirname "$SOURCE"  )" && pwd )"
done
BIN_DIR="$( cd -P "$( dirname "$SOURCE" )" && pwd )"

if [[ $# -lt 3 ]]
then
  echo $USAGE
  exit 1
fi

rundate=$(date +%s)

inventory=$1
password=$2
dnssuffix=$3
domain=$(echo $dnssuffix | awk -F. '{print $(NF-1)}')
tld=$(echo $dnssuffix | awk -F. '{print $(NF)}')

# find stuff relative to where I am
INVENTORY_NAME="${1%.*}"
LIB_DIR=$BIN_DIR/lib
PKI_DIR=$LIB_DIR/${INVENTORY_NAME}/pki
CLUSTER_DIR=$LIB_DIR/${INVENTORY_NAME}

#defaul dname completion for the certs. Will be replaced with value in *-tls-aliases
cust_dname_completion="ou=edh,dc=${domain},dc=${tld}"

# declare the SAN_MAP variable for setting subject alternate names in the csr
# Becaue this uses an asociative array, bash 4.x is required

# load customer tls values
declare -A SAN_MAP
if [[ -f "${INVENTORY_NAME}-tls-aliases" ]]
then
  source "${INVENTORY_NAME}-tls-aliases"
fi

# define and initialize customer certificate info
cust_ca_cert_dir=$LIB_DIR/ca_certs
cust_node_cert_dir=$CLUSTER_DIR/cust_node_certs

cust_ca_cert_pem_list=""
cust_ca_cert_der_list=""
cust_node_cert_pem_list=""

# self signed cluster root certificate info
clusterCA_jks_ss=${LIB_DIR}/cluster_certs/${INVENTORY_NAME}_cluster_CA-ss.jks
clusterCA_ss_pem=${LIB_DIR}/cluster_certs/${INVENTORY_NAME}_cluster_CA-ss.pem
clusterCA_ss_cer=${LIB_DIR}/cluster_certs/${INVENTORY_NAME}_cluster_CA-ss.cer
clusterCA_key=${LIBR_DIR}/cluster_certs/${INVENTORY_NAME}_cluster_CA-key.pem

cleanup() {
    rv=$?
    rm -rf "$ssltmp"
    exit $rv
}

# create tmp working file
ssltmp=$(mktemp)
rm -f $ssltmp
trap "cleanup" EXIT

# create directory structure
mkdir -p "${LIB_DIR}/cluster_certs/" "$cust_ca_cert_dir" "$cust_node_cert_dir"
mkdir -p "$PKI_DIR/jks/" "$PKI_DIR/x509/"

cat >"${LIB_DIR}/cluster_certs/README.md" <<EOF
# Cluster Certificates

This directory holds the self-signed cluster certificate that we use to sign
the 'self-signed' node certificates for the cluster.
EOF

cat >"$cust_ca_cert_dir/README.md" <<EOF
# Customer CA Certificates

Place all customer provided root/intermediate CA certificates that need to be loaded into
the cluster truststore in this directory.

The script will add any cluster_ca certs to this directory

Certificate files should be:
  in DER format with a .cer extension
or
  in PEM format with a .pem extension.

Script will convert certs as needed so that all certs have both a DER & PEM encoded file.
EOF

cat >"$cust_node_cert_dir/README.md" <<EOF
# Customer Node Certificates

Place all customer provided node certificates for the cluster in this dorectory.

Certificate files should be in PEM format with a .pem extension.
EOF


# generate self signed cluster root pair
ca_pkname=${INVENTORY_NAME}_cluster_ca
# check for cluster CA cert and build if needed
if [[ ! -f $clusterCA_jks_ss ]]
then
  # generate cluster root key & cert
  # any reason to make root cert stronger or last longer in this senario?
  keytool -genkey -alias "$ca_pkname" -keystore "$clusterCA_jks_ss" \
          -dname "cn=${ca_pkname},${cust_dname_completion}" \
          -storepass "$password" -keypass "$password" \
          -ext basicConstraints:critical=CA:true \
          -ext keyUsage:critical=digitalSignature,cRLSign,keyCertSign \
          -keyalg RSA  -keysize 2048 -validity 720
  # export the cert
  keytool -exportcert -alias "$ca_pkname" -keystore "$clusterCA_jks_ss" \
          -file "$clusterCA_ss_cer" \
          -storepass "$password" -keypass "$password"
  keytool -exportcert -alias "$ca_pkname" -keystore "$clusterCA_jks_ss" \
          -file "$clusterCA_ss_pem" \
          -storepass "$password" -keypass "$password" -rfc
  # export the key
  rm -f $ssltmp
  keytool -importkeystore \
          -srcalias "$ca_pkname" -srckeystore "$clusterCA_jks_ss" \
          -srcstorepass "$password" -srckeypass "$password" \
          -destkeystore "$ssltmp" -deststoretype PKCS12 \
          -deststorepass "$password" -destkeypass "$password"
  openssl pkcs12 -in "$ssltmp" -passin pass:"$password" -nocerts -out "$clusterCA_key" -passout pass:"$password"
fi
# add cluster cert to ca_certs directory
cp -p $clusterCA_ss_cer $cust_ca_cert_dir/
cp -p $clusterCA_ss_pem $cust_ca_cert_dir/

# convert certs in cust_ca so we have both DER & PEM encoded certs
# DER to PEM
if [[ "$(ls -A $cust_ca_cert_dir/*.cer)" ]]
then
  for ca_cert_der in $cust_ca_cert_dir/*.cer
  do
    ca_cert_pem=${ca_cert_der%.cer}.pem
    if [[ ! -f ${ca_cert_pem} ]]
    then
      openssl x509 -inform der -outform pem -in $ca_cert_der -out $ca_cert_pem
    fi
  done
fi
# PEM to DER
if [[ "$(ls -A $cust_ca_cert_dir/*.pem)" ]]
then
  for ca_cert_pem in $cust_ca_cert_dir/*.pem
  do
    ca_cert_der=${ca_cert_pem%.pem}.cer
    if [[ ! -f ${ca_cert_der} ]]
    then
      openssl x509 -inform pem -outform der -in $ca_cert_pem -out $ca_cert_der
    fi
  done
fi

# populate customer cert file lists
if [[ "$(ls -A $cust_ca_cert_dir/*.pem)" ]]
then
  cust_ca_cert_pem_list="$(ls -A $cust_ca_cert_dir/*.pem)"
fi
if [[ "$(ls -A $cust_node_cert_dir/*.pem)" ]]
then
  cust_node_cert_pem_list="$(ls -A $cust_node_cert_dir/*.pem)"
fi

# copy any current customer supplied host certs to the pki/x509 directory
if [[ ! -z "$cust_node_cert_pem_list" ]]
then
  for cust_node_cert in $cust_node_cert_pem_list
  do
    cp -p $cust_node_cert $PKI_DIR/x509/
  done
fi

# process certs for all the hostnames in inventory file
HOSTS=$(grep $dnssuffix $BIN_DIR/$inventory | perl -pe 's@.*?([A-z0-9-\.]+\'"$dnssuffix"').*@$1@g' | sort -u)

for host in $HOSTS
do
  pkname=${host}
  # keystore with real cert
  jks=$PKI_DIR/jks/${host}.jks
  # keystore with ss cert
  jks_ss=$PKI_DIR/jks/${host}-ss.jks
  # real cert
  cert_pem=$PKI_DIR/x509/${host}.pem
  # ss cert
  cert_ss_pem=$PKI_DIR/x509/${host}-ss.pem
  key="${cert_pem%.pem}.key"
  csr="${cert_pem%.pem}.csr"
  ext="${cert_pem%.pem}.ext"

  sans=${SAN_MAP["${host}"]}

  # generate a key if the jks doesn't exist
  if [[ ! -f $jks_ss ]]
  then
    if [[ "$sans" != "" ]]
    then
      # create cert with subject alt names
      keytool -genkey \
              -alias "$pkname" -keystore "$jks_ss" \
              -dname "cn=${host},${cust_dname_completion}" \
              -storepass "$password" -keypass "$password" \
              -keyalg RSA -keysize 2048 -validity 720 \
              -ext "SAN=$sans"
    else
      keytool -genkey \
              -alias "$pkname" -keystore "$jks_ss" \
              -dname "cn=${host},${cust_dname_completion}" \
              -storepass "$password" -keypass "$password" \
              -keyalg RSA -keysize 2048 -validity 720 \
              -ext "SAN=dns:${host}"

    fi
  fi

  # export the key
  # Reference: http://www.cloudera.com/documentation/enterprise/latest/topics/cm_sg_openssl_jks.html#concept_ek3_sdl_rp
  if [[ ! -f $key ]]
  then
    rm -f $ssltmp
    keytool -importkeystore \
            -srcalias "$pkname" -srckeystore "$jks_ss" \
            -srcstorepass "$password" -srckeypass "$password" \
            -destkeystore "$ssltmp" -deststoretype PKCS12 \
            -deststorepass "$password" -destkeypass "$password"
    openssl pkcs12 -in "$ssltmp" -passin pass:"$password" -nocerts -out "$key" -passout pass:"$password"
  fi

  # generate the csr for the host
  if [[ ! -f $csr ]]
  then
    if [[ "$sans" != "" ]]
    then
      # create csr with subject alt names
      keytool -certreq \
              -alias "$pkname" -keystore "$jks_ss" \
              -storepass "$password" -keypass "$password" \
              -file "$csr" \
              -ext EKU=serverAuth,clientAuth \
              -ext "SAN=$sans"
    else
      keytool -certreq \
              -alias "$pkname" -keystore "$jks_ss" \
              -storepass "$password" -keypass "$password" \
              -file "$csr" \
              -ext EKU=serverAuth,clientAuth \
              -ext "SAN=dns:${host}"
    fi
  fi

  # generate signed certs...
  if [[ ! -f $cert_ss_pem ]]
  then
    if [[ "$sans" != "" ]]
    then
      keytool -gencert -rfc \
              -infile "$csr" -outfile "$cert_ss_pem" \
              -keystore "$clusterCA_jks_ss" -alias "$ca_pkname" -storepass "$password" \
              -validity 720 \
              -ext EKU=serverAuth,clientAuth \
              -ext "SAN=$sans"
    else
      keytool -gencert -rfc \
              -infile "$csr" -outfile "$cert_ss_pem" \
              -keystore "$clusterCA_jks_ss" -alias "$ca_pkname" -storepass "$password" \
              -validity 720 \
              -ext EKU=serverAuth,clientAuth \
              -ext "SAN=dns:${host}"
    fi
    # add cluster_cert to make it complete chain
    cat "$clusterCA_ss_pem" >> "$cert_ss_pem"
  fi

  # rebuild all trusted certs in host keystore
  # rebuild trusted certs - remove all certs except the host key pair
  existing_certs="$(keytool -list -rfc -keystore $jks_ss -storepass $password | egrep '^Alias name:'| cut -d' ' -f 3)"
  for exist_pkname in $existing_certs
  do
    if [[ $exist_pkname != $pkname ]]
    then
      keytool -delete -alias "$exist_pkname" -keystore "$jks_ss" -storepass "$password"
    fi
  done

  # rebuild trusted certs - import all root/intermediate certs
  if [[ ! -z "$cust_ca_cert_pem_list" ]]
  then
    for chain_cert in $cust_ca_cert_pem_list
    do
      if [[ -f $chain_cert ]]
      then
        cust_pkname=${chain_cert##*/}
        keytool -importcert \
                -alias "$cust_pkname" -keystore "$jks_ss" \
                -file "$chain_cert" \
                -storepass "$password" \
                -noprompt
      fi
    done
  fi

  # import cluster signed cert into keystore
  keytool -importcert -keystore "$jks_ss" -alias "$pkname" -file "$cert_ss_pem" -storepass "$password" -noprompt

  # archive any old customer cert based jks keystore
  test -f "$jks" && mv -f "$jks" "${jks}.${rundate}.old"

  # if customer has supplied a cert for this host, generate non-ss jks
  if [[ -f $cert_pem ]]
  then
    # For the customer jks, start with the jks-ss (which contains the private key and cluster/root/intermediate certs)
    cp "$jks_ss" "$jks"
    # import customer host cert and trap error if keytool cannot validate host cert
    #  - it will not be able to validate the host cert if the full trust chain for the cert is not in the jks
    import_error=0
    keytool -importcert \
            -alias "$pkname" -keystore "$jks" \
            -file "$cert_pem" \
            -storepass "$password" \
            -trustcacerts -noprompt \
      || import_error=1
    if [[ $import_error -eq 1 ]]
    then
      # host cert import failed, tag cust pem and cust cert based jks
      mv -f "$jks" "${jks}.${rundate}.problem"
      mv -f "$cert_pem" "${cert_pem}.${rundate}.problem"
    fi
  fi
done

# define/clear truststore
truststore_pem=$PKI_DIR/x509/truststore.pem
rm -f $truststore_pem
# add cluster root and any customer supplied certs to the truststore
for trust_cert in $cust_ca_cert_pem_list
do
  cat $trust_cert >> $truststore_pem
  echo >> $truststore_pem
done

# Take the default cacerts for java 8, then import all root and intermediate certs
# this will later be deployed to all jvms on the hosts so that the trust stores
# for java procs don't need to be configured
# -for loop allows us to process both/either java 7 & 8 cacerts
for cacert in $LIB_DIR/cacerts-java-1.8
do
  cacert_dest=$PKI_DIR/jks/jsse$(basename ${cacert}).jks
  cp -f $cacert $cacert_dest
  for chain_pem in $cust_ca_cert_pem_list
  do
    i=0
    if [[ -f $chain_pem ]]
    then
      chain_pkname=${chain_pem##*/}
      keytool -importcert \
              -alias "$chain_pkname-$i-$rundate" -keystore "$cacert_dest" \
              -file "$chain_pem" \
              -storepass changeit \
              -noprompt
      ((i++)) || true # required due to set -e
    fi
  done
done


