#!/bin/sh
VAULT_PASSPHRASE=~/.ansible_vault_passphrase.gpg
if [[ -n "$CHECK_SYNTAX" ]]
then
  echo syntax-check-only-passphrase-does-not-matter
elif [[ -f $VAULT_PASSPHRASE ]]
then
  gpg --batch --use-agent --decrypt $VAULT_PASSPHRASE
elif [[ -n $ANSIBLE_VAULT_PASSWORD ]]
  then
    echo $ANSIBLE_VAULT_PASSWORD    
else
  echo -n "Vault Passphrase: " 1>&2
  read -s VAULT_PASSPHRASE
  echo $VAULT_PASSPHRASE
fi

