import sys
import re
from ruamel.yaml import YAML, YAMLError

sensitive_config = {
'KDC_ADMIN_PASSWORD' : '{{ PLACEHOLDER }}',
'KEYSTORE_PASSWORD' : '{{ PLACEHOLDER }}',
'LDAP_BIND_PW' : '{{ PLACEHOLDER }}',
'LICENSE_KEY' : '{{ PLACEHOLDER }}',
'impala_cmd_args_safety_valve' : '{{ PLACEHOLDER }}',
'llama_am_ha_zk_auth_secret_key' : '{{ PLACEHOLDER }}',
'ssl_private_key_password' : '{{ PLACEHOLDER }}',
'bind_password' : '{{ PLACEHOLDER }}',
'database_password' : '{{ PLACEHOLDER }}',
'hue_service_safety_valve' : '{{ PLACEHOLDER }}',
'spark-conf/spark-env.sh_service_safety_valve' : '{{ PLACEHOLDER }}',
'core_site_safety_valve' : '{{ PLACEHOLDER }}',
'dfs_ha_fencing_cloudera_manager_secret_key' : '{{ PLACEHOLDER }}',
'fc_authorization_secret_key' : '{{ PLACEHOLDER }}',
'http_auth_signature_secret' : '{{ PLACEHOLDER }}',
'ssl_server_keystore_keypassword' : '{{ PLACEHOLDER }}',
'ssl_server_keystore_password' : '{{ PLACEHOLDER }}',
'ssl_server_keystore_keypassword' : '{{ PLACEHOLDER }}',
'ssl_server_keystore_password' : '{{ PLACEHOLDER }}',
'zk_authorization_secret_key' : '{{ PLACEHOLDER }}',
'hive_metastore_database_password' : '{{ PLACEHOLDER }}',
'hive_service_config_safety_valve' : '{{ PLACEHOLDER }}',
'hiveserver2_keystore_password' : '{{ PLACEHOLDER }}',
'ARCADIAENTERPRISE_service_env_safety_valve' : '{{ PLACEHOLDER }}',
'metastore_db_password' : '{{ PLACEHOLDER }}',
'STREAMSETS_service_env_safety_valve' : '{{ PLACEHOLDER }}',
'sentry_server_database_password' : '{{ PLACEHOLDER }}',
'cdsw.kubernetes.secret' : '{{ PLACEHOLDER }}',
'log4j_safety_valve' : '{{ PLACEHOLDER }}',
'oozie_config_safety_valve' : '{{ PLACEHOLDER }}',
'oozie_database_password' : '{{ PLACEHOLDER }}',
'catalogd_cmd_args_safety_valve' : '{{ PLACEHOLDER }}',
'webserver_private_key_password_cmd' : '{{ PLACEHOLDER }}',
'webserver_private_key_password_cmd' : '{{ PLACEHOLDER }}',
'webserver_private_key_password_cmd' : '{{ PLACEHOLDER }}',
'kafka.properties_role_safety_valve' : '{{ PLACEHOLDER }}',
'ssl_client_truststore_password' : '{{ PLACEHOLDER }}',
'ssl_server_keystore_keypassword' : '{{ PLACEHOLDER }}',
'ssl_server_keystore_password' : '{{ PLACEHOLDER }}',
'hue_server_hive_safety_valve' : '{{ PLACEHOLDER }}',
'ssl_private_key_password' : '{{ PLACEHOLDER }}',
'httpfs_config_safety_valve' : '{{ PLACEHOLDER }}',
'log4j_safety_valve' : '{{ PLACEHOLDER }}',
'resourcemanager_config_safety_valve' : '{{ PLACEHOLDER }}',
'ssl_server_keystore_password' : '{{ PLACEHOLDER }}',
'ssl_server_keystore_password' : '{{ PLACEHOLDER }}',
'ARCENGINE_role_env_safety_valve' : '{{ PLACEHOLDER }}',
'dpm.password' : '{{ PLACEHOLDER }}',
'https.cluster.keystore.password' : '{{ PLACEHOLDER }}',
'https.keystore.password' : '{{ PLACEHOLDER }}',
'ldap.bindPassword' : '{{ PLACEHOLDER }}',
'sdc-env.sh_role_safety_valve' : '{{ PLACEHOLDER }}',
'sdc-security.policy_role_safety_valve' : '{{ PLACEHOLDER }}',
'sdc.properties_role_safety_valve' : '{{ PLACEHOLDER }}',
'ssl_server_keystore_password' : '{{ PLACEHOLDER }}',
'gflagfile_role_safety_valve' : '{{ PLACEHOLDER }}',
'gflagfile_role_safety_valve' : '{{ PLACEHOLDER }}',
'cloudera_trustee_keyprovider_auth' : '{{ PLACEHOLDER }}',
'kms-acls.xml_role_safety_valve' : '{{ PLACEHOLDER }}',
'ssl_client_truststore_password' : '{{ PLACEHOLDER }}',
'ssl_server_keystore_password' : '{{ PLACEHOLDER }}',
'cloudera_trustee_keyprovider_auth' : '{{ PLACEHOLDER }}',
'kms-acls.xml_role_safety_valve' : '{{ PLACEHOLDER }}',
'ssl_client_truststore_password' : '{{ PLACEHOLDER }}',
'ssl_server_keystore_password' : '{{ PLACEHOLDER }}',
'heimdali_admin_password' : '{{ PLACEHOLDER }}',
'heimdali_db_pass' : '{{ PLACEHOLDER }}',
'heimdali_ldap_admin_pass' : '{{ PLACEHOLDER }}',
'alert-engine.yml_role_safety_valve' : '{{ PLACEHOLDER }}',
'smtp.password' : '{{ PLACEHOLDER }}',
'collection-roller.yml_role_safety_valve' : '{{ PLACEHOLDER }}',
'cluster.cm.password' : '{{ PLACEHOLDER }}',
'mail.password' : '{{ PLACEHOLDER }}',
'ssl_server_privatekey_password' : '{{ PLACEHOLDER }}',
'ssl_server_privatekey_password' : '{{ PLACEHOLDER }}',
'alert_mailserver_password' : '{{ PLACEHOLDER }}',
'log4j_safety_valve' : '{{ PLACEHOLDER }}',
'navigator_database_password' : '{{ PLACEHOLDER }}',
'nav_ldap_bind_pw' : '{{ PLACEHOLDER }}',
'nav_metaserver_database_password' : '{{ PLACEHOLDER }}',
'ssl_server_keystore_keypassword' : '{{ PLACEHOLDER }}',
'ssl_server_keystore_password' : '{{ PLACEHOLDER }}',
'headlamp_database_password' : '{{ PLACEHOLDER }}'
}


class CmApiToYaml():
    def __init__(self):
        # If typ='safe' is set then YAML gets ordered alphabetically which makes configs easy to read/find
        # If not set the load preserves order but not configs, making it hard to find name of ansible section
        self.yaml = YAML()
        self.yaml.default_flow_style = False
        self.yaml.width = 4096                # Needed to prevent URI line from being wrapped on output.
        self.template = ""
        self.yaml_configs = ""
        with open("data/cmapi.yml") as stream:
            try:
                self.ansible_cm_configs = self.yaml.load(stream)
            except YAMLError as exc:
                print(exc)

    def ansible_template_to_yaml(self, ansible_dict):
        self.template = """
        - name: {}
          cm_config:
            uri: "{{{{ cm_protocol }}}}://{{{{ groups['manager'][0] }}}}:{{{{ cm_port }}}}/api/{{{{ cm_api_version }}}}/{}"
            username: "{{{{ cm_admin }}}}"
            password: "{{{{ cm_admin_password }}}}"
            configs:
        """.format(ansible_dict['name'],
                   ansible_dict['cm_api_path'])
        self.yaml_configs = self.yaml.load(self.template)

    def dict_to_yaml(self, cmapi_configs):
        self.yaml_configs[0]['cm_config']['configs'] = {}
        for config in cmapi_configs:
            if (config['sensitive'] or re.search('safety', config['name'], re.IGNORECASE)) and\
                    config['name'] not in sensitive_config:   # Alerting when Sensitive fields not accounted masked
                print('\033[91m' + "Sensitive Data Field Detected: {}".format(config['name']) + '\033[0m')
                self.yaml_configs[0]['cm_config']['configs'][config['name']] = sensitive_config[config['name']]
            else:
                self.yaml_configs[0]['cm_config']['configs'][config['name']] = config['value']

    def print_yaml(self):
        self.yaml.dump(self.yaml_configs, sys.stdout)

    def save_yaml(self, file_name):
        with open('tasks/{}.yml'.format(file_name), 'w') as outfile:
            self.yaml.dump(self.yaml_configs, outfile)
