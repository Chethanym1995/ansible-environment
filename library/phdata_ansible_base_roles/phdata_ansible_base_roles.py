#!/usr/bin/python

ANSIBLE_METADATA = {
    'metadata_version': '1.0',
    'status': ['preview'],
    'supported_by': 'phdata'
}

DOCUMENTATION = '''
---
module: phdata_base_download
short_description: List available Ansible roles, their versions, and download/install.
version_added: "2.7.5"
description:
    - "Updated Cloudera Manager via the CM API. While the uri module can be used, this module supports change capture and reduces configuration."
options:
  baseurl:
    description:
      - Url to parse data from (repo)
    required: true
  command:
    description:
      - list-roles, get-roles-all, or get-roles commands
    required: true
  authkey:
    description:
      - password/key to decrypt tarball
    required: true
'''

EXAMPLES = '''
groupvars__phdata_ansible_base_roles:
  - { name: 'common', version: '1.0.4' }
  - { name: 'active-directory', version: '1.0.1' }
  - { name: 'cloudera-node', version: '1.0.0' }

- name: Latest Roles Available
  phdata_ansible_base_roles:
    baseurl: https://phdata-ansible-release.s3.amazonaws.com/
    command: list-roles
    authkey: "{{ vaultvars__ansible_setup_authkey }}"
  register: roles_available
  tags: ['never','list-roles']

- name: Latest Roles Available Output
  debug:
    msg: "{{ roles_available.output  }}"
  tags: ['never','list-roles']

- name: Install Roles - All
  phdata_ansible_base_roles:
    baseurl: https://phdata-ansible-release.s3.amazonaws.com/
    command: get-roles-all
    authkey: "{{ vaultvars__ansible_setup_authkey }}"
  tags: ['never','get-roles-all']

- name: Install Roles - Specific
  phdata_ansible_base_roles:
    baseurl: https://phdata-ansible-release.s3.amazonaws.com/
    command: get-roles
    authkey: "{{ vaultvars__ansible_setup_authkey }}"
    roles: "{{ groupvars__phdata_ansible_base_roles }}"

- name: Validate the versions of roles installed
  phdata_ansible_base_roles:
    command: validate-roles
    authkey: "{{ vaultvars__ansible_setup_authkey }}"
    roles: "{{ groupvars__phdata_ansible_base_roles }}"
'''

RETURN = '''
None
'''

from ansible.module_utils.basic import AnsibleModule, get_exception
from ansible.module_utils.urls import open_url

import urllib
import re
import requests
import os
from os.path import expanduser
import subprocess
from xml.dom.minidom import parseString

def match_role_version(value):
  match = re.search('/?([A-Za-z\d_-]+)-([0-9\.]+)', value)
  if not match:
    raise Exception("Did not match role-version in {}".format(value))
  if len(match.groups()) != 2:
    raise Exception("Did not match role-version in {}".format(value))
  return match

def validate_role_name(role_name, releases):
  if role_name not in releases.keys():
    raise Exception("Role '{}' not found in: [{}]".format(role_name, ",".join(releases.keys())))

def validate_role_version(role_name, role_version, releases):
  validate_role_name(role_name, releases)
  if role_version not in releases[role_name].keys():
    raise Exception("Role version '{}' not found in: [{}]".format(role_version, ",".join(releases[role_name].keys())))

def get_role(releases, baseurl, location, role, key):
    match = match_role_version(role)
    role_name = match.group(1)
    role_version = match.group(2)
    validate_role_name(role_name, releases)
    validate_role_version(role_name, role_version, releases)
    files = releases[role_name][role_version]
    if len(files) != 3:
      raise Exception("Expected three files, found [{}]".format(",".join(files)))
    os.chdir(location)

    # Do we already have this role
    files_in_dir = os.listdir(location)
    if role not in files_in_dir:
        for f in files:
          urllib.urlretrieve(baseurl + f, os.path.basename(f))
        print("Decrypting tar")
        subprocess.check_call("openssl enc -d -aes256 -in {}.tar.enc -out {}.tar -pass pass:{}".format(role, role, key), shell=True)
        print("Decrypting git log")
        subprocess.check_call("openssl enc -d -aes256 -in {}.txt.enc -out {}.txt -pass pass:{}".format(role, role, key), shell=True)
        print("Remove the encrypted files")
        subprocess.check_call("rm *.enc", shell=True)
        print("Checking checksum")
        subprocess.check_call("sha256sum --quiet -c < {}.tar.sha256".format(role), shell=True)
        print("Extracting tar")
        subprocess.check_call("tar -xmf {}.tar".format(role), shell=True)

    print("Installing symlink")
    if os.path.islink(role_name):
        subprocess.check_call("rm {}".format(role_name), shell=True)
    subprocess.check_call("ln -sf {} {}".format(role, role_name), shell=True)
    print("Successfully installed {} as the role {}".format(role, role_name))

def run_module():
    # define the available arguments/parameters that a user can pass to
    # the module
    module_args = dict(
        authkey=dict(required=True),
        baseurl=dict(required=True),
        command=dict(required=True),
        roles=dict(type="list")
    )

    # seed the result dict in the object
    # we primarily care about changed and state
    # change is if this module effectively modified the target
    # state will include any data that you want your module to pass back
    # for consumption, for example, in a subsequent task
    result = dict(
        changed=False,
        changed_configs={},
        output={}
    )

    # the AnsibleModule object will be our abstraction working with Ansible
    # this includes instantiation, a couple of common attr would be the
    # args/params passed to the execution, as well as if the module
    # supports check mode
    module = AnsibleModule(
        argument_spec=module_args,
        supports_check_mode=True
    )

    location = expanduser("~") + '/.ansible_phdata/roles/'

    subprocess.check_call("mkdir -p {}".format(location), shell=True)

    # We need to gather all of our current symlinks if there are any.
    current_symlinks = {}
    for name in os.listdir(location):
        full = os.path.join(location, name)
        if os.path.islink(full):
            current_symlinks[name] = {}
            current_symlinks[name]['path'] = os.readlink(full)
            current_symlinks[name]['version'] = os.readlink(full).split("-")[-1]

    result['debug'] = current_symlinks
    # build release tree
    list_url = module.params['baseurl'] + "?max-keys=10000"
    response = requests.get(list_url)
    dom = parseString(response.content)
    releases = {}
    for key in dom.getElementsByTagName('Key'):
        value = key.firstChild.nodeValue
        match = match_role_version(value)
        role_name = match.group(1)
        role_version = match.group(2)
        if role_name not in releases:
          releases[role_name] = {}
        if role_version not in releases[role_name]:
          releases[role_name][role_version] = []
        releases[role_name][role_version].append(value)

    if module.params['command'] == 'list-roles':
        # list out all of our available roles
        tmp = {}
        for role in releases:
            tmp[role] = {}
            tmp[role]['latest'] = sorted(releases[role], reverse = True)[0]
            if role in current_symlinks:
                tmp[role]['installed'] = current_symlinks[role]['version']
            else:
                tmp[role]['installed'] = None
        result['output'] = tmp

    if module.params['command'] == 'get-roles':
        # loop through our roles and versions and download/symlink
        for role in module.params['roles']:
            get_role(releases, module.params['baseurl'], location, role['name'] + '-' + role['version'], module.params['authkey'])

    if module.params['command'] == 'get-roles-all':
        # get every role at the latest version
        for role in releases:
            latest = sorted(releases[role], reverse = True)[0]
            get_role(releases, module.params['baseurl'], location, role + '-' + latest, module.params['authkey'])

    if module.params['command'] == 'validate-roles':
        # validate-roles will loop through our list of roles and versions to
        # make sure that those are the symlinked versions or error/fail
        for role in module.params['roles']:
            full_role_name = role['name'] + '-' + role['version']

            latest = sorted(releases[role['name']], reverse = True)[0]
            if latest > role['version']:
                module.warn(full_role_name + ' IS OUT OF DATE, LATEST IS: ' + latest)

            os.chdir(location)
            files_in_dir = os.listdir(location)
            # has the role and version been downloaded yet?
            if full_role_name not in files_in_dir:
                module.fail_json(msg='This role is not available: {}'.format(str(full_role_name)))
            else:
                # if a symlink already exists, delete it first
                if os.path.islink(role['name']):
                    subprocess.check_call("rm {}".format(role['name']), shell=True)
                # create the symlink
                subprocess.check_call("ln -sf {} {}".format(full_role_name, role['name']), shell=True)

    # in the event of a successful module execution, you will want to
    # simple AnsibleModule.exit_json(), passing the key/value results
    if not result['debug']:
       del result['debug']
    module.exit_json(**result)

def main():
    run_module()

if __name__ == '__main__':
    main()
