# `phdata_ansible_base_roles`  

## Overview  
This module helps us maintain our ansible role deployments from within ansible itself.  

We download and symlink roles within `~/.ansible_phdata/roles/`. This location is included in `.ansible.cfg` -> `roles_path    = roles:~/.ansible_phdata/roles/`

## Setup   
#### Vault vars  
`vaultvars__ansible_setup_authkey: <key from lastpass>`  
Note: for encryption key/authkey, see ANSIBLE_RELEASE_ENCRYPTION_KEY in Lastpass under ansible-base Test and Release.  

#### Group vars  
```
groupvars__phdata_ansible_base_roles_baseurl: https://phdata-ansible-release.s3.amazonaws.com/
# List of all the ansible-base-* roles we want to deploy
# note: these should be copied/move to cluster/env specific directories
groupvars__phdata_ansible_base_roles:
  - { name: 'common', version: '1.0.4' }
  - { name: 'active-directory', version: '1.0.1' }
  - { name: 'cloudera-node', version: '1.0.0' }
```

#### ansible.cfg  
We need to update the paths accepted for roles:  
`roles_path    = roles:~/.ansible_phdata/roles/`  

## Bootstrap  
site_bootstrap.yml:    

```
---
- name: Ansible Roles Setup
  hosts: localhost
  connection: local
  tasks:
    - name: tags help...
      debug:
        msg: "--tags options: list-roles, get-roles-all, get-roles"
        
    - name: Latest Roles Available
      phdata_ansible_base_roles:
        baseurl: "{{ groupvars__phdata_ansible_base_roles_baseurl }}"
        command: list-roles
        authkey: "{{ vaultvars__ansible_setup_authkey }}"
      register: roles_available
      tags: ['never','list-roles']

    - name: Latest Roles Available Output
      debug:
        msg: "{{ roles_available.output  }}"
      tags: ['never','list-roles']

    - name: Install Roles - All
      phdata_ansible_base_roles:
        baseurl: "{{ groupvars__phdata_ansible_base_roles_baseurl }}"
        command: get-roles-all
        authkey: "{{ vaultvars__ansible_setup_authkey }}"
      tags: ['never','get-roles-all']

    - name: Install Roles - Specific
      phdata_ansible_base_roles:
        baseurl: "{{ groupvars__phdata_ansible_base_roles_baseurl }}"
        command: get-roles
        authkey: "{{ vaultvars__ansible_setup_authkey }}"
        roles: "{{ groupvars__phdata_ansible_base_roles }}"
      tags: ['never','get-roles']

```

`$ ansible-playbook site_bootstrap.yml`  

```
...
TASK [tags help...] *****************************************************************************************************************************************************************
ok: [localhost] => {
    "msg": "--tags options: list-roles, get-roles-all, get-roles"
}
...
```

`$ ansible-playbook site_bootstrap.yml --tags: list-roles`  
Currently display the latest version available for each role.  

`$ ansible-playbook site_bootstrap.yml --tags: get-roles`  
Using `groupvars__phdata_ansible_base_roles:` list of roles and versions and download them specifically.  


## Top of site.yml  
```
---
- name: Ansible Roles Setup
  hosts: localhost
  connection: local
  tasks:
    - name: Validate the versions of roles installed
      phdata_ansible_base_roles:
        baseurl: "{{ groupvars__phdata_ansible_base_roles_baseurl }}"
        command: validate-roles
        authkey: "{{ vaultvars__ansible_setup_authkey }}"
        roles: "{{ groupvars__phdata_ansible_base_roles }}"

... rest of your site.yml
```  

`$ ansible-playbook site.yml -k`  

We re-validate and re-create all symlinks to the appropriate values in groupvars, or fail if the version is not available (need to re-bootstrap).    

```
...
TASK [Validate the versions of roles installed] *************************************************************************************************************************************
 [WARNING]: common-1.0.4 IS OUT OF DATE, LATEST IS: 1.0.5

 [WARNING]: active-directory-1.0.1 IS OUT OF DATE, LATEST IS: 2.0.0

 [WARNING]: cloudera-node-1.0.0 IS OUT OF DATE, LATEST IS: 1.0.3
...
```