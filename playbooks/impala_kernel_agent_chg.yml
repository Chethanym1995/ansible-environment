---
- hosts: all
  become_user: root
  become_method: sudo
  become: true
  tasks:
    - name: Get Cluster details
      uri:
        url: "{{ groupvars__edh_api_protocol }}://{{ groupvars__edh_api_hostname }}:{{ groupvars__edh_api_port }}/api/{{ groupvars__edh_api_version }}/clusters/cluster"
        user: "{{ groupvars__edh_api_username }}"
        password: "{{ groupvars__edh_api_password }}"
        validate_certs: False
        body_format: json
      register: cm_version
      check_mode: no

    - name: Get Cluster version
      set_fact:
        version : "{{ cm_version.json.fullVersion }}"
      check_mode: no
    
    - block:
        - name: Lookup kernel.pid_max value
          command: sysctl kernel.pid_max
          register: kernel_pid_max
          check_mode: no

        - name: Lookup kernel.threads-max
          command: sysctl kernel.threads-max
          register: kernel_threads_max
          check_mode: no

        - name: Lookup vm.max_map_count
          command: sysctl vm.max_map_count
          register: vm_max_map_count
          check_mode: no

        - debug:
            msg: " {{kernel_pid_max.stdout}} {{kernel_threads_max.stdout}} {{vm_max_map_count.stdout}}"

        - name: Set kernel.pid_max
          sysctl:
            name: kernel.pid_max
            value: 2000000
            state: present
            reload: yes
          when: kernel_pid_max.stdout.split('=')[1]|int < 2000000

        - name: Set kernel.threads-max
          sysctl:
            name: kernel.threads-max
            value: 2000000
            state: present
            reload: yes
          when: kernel_threads_max.stdout.split('=')[1]|int < 2000000


        - name: Set vm.max_map_count
          sysctl:
            name: vm.max_map_count
            value: 8000000
            state: present
            reload: yes
          when: vm_max_map_count.stdout.split('=')[1]|int < 8000000

        - name: Modify/Add nproc soft limit
          pam_limits:
            domain: impala
            limit_type: soft
            limit_item: nproc
            value: 524288

        - name: Modify/Add nproc hard limit
          pam_limits:
            domain: impala
            limit_type: hard
            limit_item: nproc
            value: 524288

        - name: "Update network file"
          lineinfile:
            path: /usr/sbin/cmf-agent
            regexp: "  ulimit -n"
            line: "  ulimit -n 262144"
            backup: yes
            backrefs: yes

        - name: "Update network file"
          lineinfile:
            dest=/usr/sbin/cmf-agent
            line="  ulimit -u 524288"
            regexp="  ulimit -u"
            backrefs=yes
            backup=yes

        - name: Modify rlimit_fds for IMPALAD
          uri:
            url: "{{ groupvars__edh_api_protocol }}://{{ groupvars__edh_api_hostname }}:{{ groupvars__edh_api_port }}/api/{{ groupvars__edh_api_version }}/clusters/cluster/services/impala/roleConfigGroups/impala-IMPALAD-BASE/config"
            user: "{{ groupvars__edh_api_username }}"
            password: "{{ groupvars__edh_api_password }}"
            validate_certs: False
            body_format: json
            method: PUT
            body: { "items": [ { "name" : "rlimit_fds","value" : "1262144" } ] }
          check_mode: no

        - name: Modify rlimit_fds for STATESTORE
          uri:
            url: "{{ groupvars__edh_api_protocol }}://{{ groupvars__edh_api_hostname }}:{{ groupvars__edh_api_port }}/api/{{ groupvars__edh_api_version }}/clusters/cluster/services/impala/roleConfigGroups/impala-STATESTORE-BASE/config"
            user: "{{ groupvars__edh_api_username }}"
            password: "{{ groupvars__edh_api_password }}"
            validate_certs: False
            body_format: json
            method: PUT
            body: { "items": [ { "name" : "rlimit_fds","value" : "1262144" } ] }
          check_mode: no

        - name: Modify rlimit_fds for CATALOGSERVER
          uri:
            url: "{{ groupvars__edh_api_protocol }}://{{ groupvars__edh_api_hostname }}:{{ groupvars__edh_api_port }}/api/{{ groupvars__edh_api_version }}/clusters/cluster/services/impala/roleConfigGroups/impala-CATALOGSERVER-BASE/config"
            user: "{{ groupvars__edh_api_username }}"
            password: "{{ groupvars__edh_api_password }}"
            validate_certs: False
            body_format: json
            method: PUT
            body: { "items": [ { "name" : "rlimit_fds","value" : "1262144" } ] }
          check_mode: no
        
        - name: Cloudera agent hard stop
          command: service cloudera-scm-agent next_stop_hard

        - name: Cloudera agent restart
          command: service cloudera-scm-agent restart        
      when: version is version('5.15', '<')
