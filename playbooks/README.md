# Playbooks
These playbooks are designed to ran from the command line in ad-hoc fashion. They are expected to be 100% generic as phData will import their own playbooks into this directory. Customer specific playbooks should be placed in the root ansible directory.

## all
* Proxy configuration
Define `http_proxy` and `https_proxy` and then re-use them on the CLI

```
-e "http_proxy=$http_proxy" -e "https_proxy=$https_proxy"
```

## system_prereqs.yml
### Variables
*  jdk_download_url
Define the URL to the java RPM resource
