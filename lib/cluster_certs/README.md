# Cluster Certificates

This directory holds the self-signed cluster certificate that we use to sign
the 'self-signed' node certificates for the cluster.
