# Customer CA Certificates

Place all customer provided root/intermediate CA certificates that need to be loaded into
the cluster truststore in this directory.

The script will add any cluster_ca certs to this directory

Certificate files should be:
  in DER format with a .cer extension
or
  in PEM format with a .pem extension.

Script will convert certs as needed so that all certs have both a DER & PEM encoded file.
