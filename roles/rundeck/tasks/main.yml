---
- name: Add Rundeck Yum repository
  yum_repository:
    name: Rundeck
    description: Rundeck Repo
    baseurl: 'https://dl.bintray.com/rundeck/rundeck-rpm'
    state: present
    gpgcheck: no

- name: release rundeck version lock
  command: yum versionlock delete rundeck*
  ignore_errors: yes

- name: Install Rundeck packages
  yum:
    name: "rundeck-{{ rundeck_version }}"
    state: present
    allow_downgrade: yes

- name: Use jaas pam authentication (through ssh/sssd). Effectively Active Directory
  template:
    src: "{{ role_path }}/files/jaas-sssd.conf"
    dest: /etc/rundeck/jaas-sssd.conf
    owner: rundeck
    group: rundeck
    mode: 0600
    backup: yes
  notify:
    - restart_rundeckd  

- name: Copy out policy file
  template:
    src: "{{ role_path }}/files/admin.aclpolicy"
    dest: /etc/rundeck/admin.aclpolicy
    owner: rundeck
    group: rundeck
    mode: 0600
    backup: yes
  notify:
    - restart_rundeckd  

- name: Use config with Mysql
  template:
    src: "{{ role_path }}/files/rundeck-config.properties.template-{{ rundeck_major_release }}"
    dest: /etc/rundeck/rundeck-config.properties
    owner: rundeck
    group: rundeck
    mode: 0600
    backup: yes
  notify:
    - restart_rundeckd

- name: copy rundeck profile
  copy:
    src: "{{ role_path }}/files/profile-{{ rundeck_major_release }}"
    dest: /etc/rundeck/profile
    owner: rundeck
    group: rundeck
    mode: 0600
    backup: yes
  notify:
    - restart_rundeckd

- name: Set up SSL properties file
  template:
    src: "{{ role_path }}/files/ssl.properties.template"
    dest: /etc/rundeck/ssl/ssl.properties
    owner: rundeck
    group: rundeck
    mode: 0600
    backup: yes
  notify:
    - restart_rundeckd

- name: Set up framework properties file
  template:
    src: "{{ role_path }}/files/framework.properties.template"
    dest: /etc/rundeck/framework.properties
    owner: rundeck
    group: rundeck
    mode: 0640
    backup: yes
  notify:
    - restart_rundeckd

- name: Copy keystore to host
  copy:
    src: "lib/{{ cluster_environment }}/pki/jks/{{ inventory_hostname }}-ss.jks"
    dest: /etc/rundeck/ssl/keystore
    owner: rundeck
    group: rundeck
    mode: 0640
    backup: yes
  when: deploy_tls_configs

- name: Copy truststore to host
  copy:
    src: "lib/{{ cluster_environment }}/pki/jks/{{ inventory_hostname }}-ss.jks"
    dest: /etc/rundeck/ssl/truststore
    owner: rundeck
    group: rundeck
    mode: 0640
    backup: yes
  when: deploy_tls_configs

- name: Ensure Rundeck is started and enabled on boot
  service:
    name: rundeckd
    state: started
    enabled: yes

- name: Lock rundeck version to avoid upgrade problems
  command: yum versionlock rundeck*

