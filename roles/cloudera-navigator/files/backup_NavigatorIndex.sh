#!/bin/bash

# The script will backup the Cloudera Navigator Solr index directory in HDFS and it deletes the files which starts with Navigator in the specified(destinationHDFSDir) folder older than the specified number of days.
# The script writes log in /var/log/cloudera-scm-navigator/Cloudera-Navigator-Index-Backup.Log

SCRIPT=$(readlink -f "$0")
ScriptRoot=$(dirname "$SCRIPT")
LogPath="/var/log/cloudera-scm-navigator/Cloudera-Navigator-Index-Backup.Log"
NoofDays="7"
    
function echolog()
{
 echo "$1"  | tee --append $LogPath > /dev/null
}


function try()
{
    [[ $- = *e* ]]; SAVED_OPT_E=$?
    set +e
}

function catch()
{
    export ex_code=$?
    (( $SAVED_OPT_E )) && set +e
    return $ex_code
}

function throwErrors()
{
    set -e
}

usage="  Usage: bash backup_NavigatorIndex.sh Destination_Navigator_index_directory_in_HDFS Source_Cloudera_Navigator_Index_Directory \\n"

if [ $# -lt 3 ]; then
  echo -e $usage
  echo -n "Enter directory path in HDFS to backup the Cloudera Navigator Index (Example: /tmp/solrbackup) > "
  read destinationHDFSDir
  echo -n "Enter Cloudera Navigator Index directory (default:/var/lib/cloudera-scm-navigator/solr) > "
  read sourceDir
  echo -n "Enter kerberos REALM > "
  read realmName
  navigatorIndexdir=${sourceDir:-'/var/lib/cloudera-scm-navigator/solr'}
  echolog "Backing up $navigatorIndexdir Index directory to HDFS $destinationHDFSDir"
  
else
    destinationHDFSDir="$1"
    navigatorIndexdir="$2"
    realmName="$3"
fi
try
{
    throwErrors
    echolog "Script Execution Start Time: $(date)"
    # Date for comparison
    now=$(date +%s);
    # Date for human readable named file name
    dateTime=$(date '+%y-%m-%d-%H-%M')
    kinit -kt "$(ls -trd /var/run/cloudera-scm-agent/process/*-hdfs-* | tail -n1)/hdfs.keytab" hdfs/$HOSTNAME@$realmName
    hdfs dfs -test -d $destinationHDFSDir || hdfs dfs -mkdir -p "$destinationHDFSDir" 
    hdfs dfs -chown hue: $destinationHDFSDir
    kdestroy
    tar --exclude='write.lock' -zcPf /tmp/Navigator-$dateTime.tgz $navigatorIndexdir
    kinit -kt "$(ls -trd /var/run/cloudera-scm-agent/process/*HUE_SERVER* | tail -n1)/hue.keytab" hue/$HOSTNAME@$realmName
    hdfs dfs -put -f "/tmp/Navigator-$dateTime.tgz" "$destinationHDFSDir/"
    rm -f /tmp/Navigator-$dateTime.tgz

    # Loop through HDFS files
    hdfs dfs -ls "$destinationHDFSDir/Navigator*" | while read f; do
    # Get File Date and File Name
    file_date=`echo $f | awk '{print $6}'`;
    file_name=`echo $f | awk '{print $8}'`;

    # Calculate Days Difference
    difference=$(( ($now - $(date -d "$file_date" +%s)) / (24 * 60 * 60) ));
    if [ $difference -gt $NoofDays ]; then
        echolog "The file $file_name is older than $difference days. Deleting $file_name!!! ";
        hdfs dfs -rm -f $file_name
    fi
    done
    echolog "Script has been executed successfully. Execution Finish Time is : $(date)"
}

catch || {

	echolog "Backing up of Cloudera Navigator Index is failed!!!!  WITH EXIT CODE $ex_code"
	exit 1
}

